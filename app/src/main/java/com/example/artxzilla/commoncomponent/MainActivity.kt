package com.example.artxzilla.commoncomponent

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.example.artxzilla.commoncomponent.databinding.ActivityMainBinding
import com.example.artxzilla.commoncomponent.model.UserObj


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        initInstance()
    }

    private fun initInstance() {
        binding.data = UserObj("art", "zilla")

        binding.btnGet.setOnClickListener({
            Toast.makeText(this, binding.data?.toString(), Toast.LENGTH_SHORT).show()
        })

        binding.btnDefault.setOnClickListener({
            binding.data = UserObj("art", "zilla")
        })
    }
}
