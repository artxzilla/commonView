package com.example.artxzilla.commoncomponent.commonview

import android.content.Context
import android.databinding.BindingAdapter
import android.databinding.InverseBindingAdapter
import android.util.AttributeSet
import android.view.View
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.TextView
import com.example.artxzilla.commoncomponent.R
import android.text.Editable
import android.text.TextWatcher
import android.databinding.InverseBindingListener



class MyCustomViewGroup : FrameLayout {
    private var mTitle: String? = null
    private var mValue: String? = null

    private var txtTitle: TextView? = null
    private var edtValue: EditText? = null

    companion object {
        @JvmStatic
        @InverseBindingAdapter(attribute = "custom_value")
        fun getCustomValue(view: MyCustomViewGroup): String {
            return view.edtValue!!.text.toString()
        }

        @JvmStatic
        @BindingAdapter("custom_value")
        fun setCustomValue(view: MyCustomViewGroup, value: String) {
            view.edtValue!!.setText(value)
        }

        @JvmStatic
        @BindingAdapter(value = "custom_valueAttrChanged")
        fun setListener(view: MyCustomViewGroup, listener: InverseBindingListener?) {
            if (listener != null) {
                view.edtValue!!.addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

                    override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

                    override fun afterTextChanged(editable: Editable) {
                        listener.onChange()
                    }
                })
            }
        }
    }

    constructor(context: Context) : super(context) {
        initInflate()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initAttribute(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initAttribute(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        initAttribute(attrs)
    }

    private fun initAttribute(attrs: AttributeSet) {
        val ta = context.obtainStyledAttributes(attrs, R.styleable.MyCustomViewGroup, 0, 0)
        try {
            mTitle = ta.getString(R.styleable.MyCustomViewGroup_custom_title)
            mValue = ta.getString(R.styleable.MyCustomViewGroup_custom_value)
        } finally {
            ta.recycle()
        }
        initInflate()
    }

    private fun initInflate() {
        View.inflate(context, R.layout.common_view, this)

        txtTitle = findViewById(R.id.txt_name)
        txtTitle!!.text = mTitle

        edtValue = findViewById(R.id.edt_value)
        edtValue!!.setText(mValue)
    }
}
